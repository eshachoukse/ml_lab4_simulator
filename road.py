import numpy as np
import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle
from matplotlib.patches import Circle
from matplotlib.animation import FuncAnimation
import random
import array
from enum import IntEnum

######  constants #########
XMIN=.35
XMAX=.65
YMIN=0.0
YMAX=1.0

n_cars = 8
n_lights = 2
n_peds = 2
n_lanes = 4

WIDTH = XMAX - XMIN
HEIGHT = YMAX - YMIN
BINS = np.arange(XMIN,XMAX+ WIDTH/n_lanes,WIDTH/n_lanes)[0:n_lanes+1]

car_size = (WIDTH/n_lanes/1.5)

DRAW_FIG = False

###### Useful functions ##########

# distance with wrapping around the Y axis
def distance(a, b):
	if abs(b - a) < HEIGHT/2:
		return b - a
	else:
		if b > a:
			return (b - a) - HEIGHT
		else:
			return HEIGHT - (a - b)

#directional distance from start to end wrapping around Y-axis
def dir_distance(start, end):
	if end > start:
		return end - start
	else:
		return HEIGHT - (start - end)

###### Class defs ##########
class ColorCode(IntEnum):
	GREEN = 0
	YELLOW = 1
	RED = 2
	BLUE = 3

class Car_Stats(IntEnum):
	LANE = 0
	XPOS = 1
	YPOS = 2
	SPEED = 3

class Closest_Car(IntEnum):
	DIST = 0
	SPEED = 1

class Closest_Ped(IntEnum):
	YDIST = 0
	XDIST = 1
	DIRECTION = 2
	
class Closest_Light(IntEnum):
	DIST = 0
	COLOR = 1

Colors = [
	[0,1,0,1], #GREEN
	[1,1,0,1], #YELLOW
	[1,0,0,1], #RED
	[0,0,1,1], #BLUE
	]

class Car:
	def __init__(self, car_id):
		global ax
		global cars
		global lane_cars
		self.car_id = car_id
		self.size = (WIDTH/n_lanes/1.5)

		self.position = [0,0]
		if car_id == 0:
			self.position[0] = n_lanes/2
		else:
			self.position[0] = random.randint(0,n_lanes-1)

		lane_cars[self.position[0]].append(self)

		self.position[1] = random.uniform(YMIN, YMAX)

		while (True):
			self.position[1] = random.uniform(YMIN, YMAX)
			NoCollide = True

			for car in cars: 
				if self.collision(car,2):
					NoCollide = False
					break
			if (NoCollide):
				break

		if (self.position[0] < n_lanes/2):
			self.direction = -1
		else:
			self.direction = +1

		if (self.position[0] == 0 or self.position[0] == n_lanes-1) and random.random() < 0.5:
			self.speed = 0.0
		else:
			self.speed = random.uniform((YMAX-YMIN)/120,(YMAX-YMIN)/60)
		self.curr_speed = self.speed
		self.distance = 0

		if (car_id == 0):
			self.color = Colors[ColorCode.RED] # our car is red
		else:
			self.color = Colors[ColorCode.BLUE] # other cars are blue

		if DRAW_FIG:
			self.rect = Rectangle(
				(XMIN + self.position[0]*(WIDTH/n_lanes) + (WIDTH/n_lanes/4), self.position[1]),
				(WIDTH/n_lanes/2),
				(WIDTH/n_lanes/1.5),
				self.size,
				facecolor=self.color,
				edgecolor = [0,0,0,1]
				)
			ax.add_artist(self.rect)
			rx = XMIN + self.position[0]*(WIDTH/n_lanes) + (WIDTH/n_lanes/4)
			ry = self.position[1] 
			self.annotation = ax.text( rx+(WIDTH/n_lanes/6), ry+(WIDTH/n_lanes/4), str(self.car_id), color = 'w')

	def __del__(self):
		if DRAW_FIG:
			self.rect.remove()

	def update(self):
		global cars

		self.curr_speed = self.speed
	
		if self.car_id != 0:
			for car in cars:
				if self.position[0] == car.position[0]:
					dist_to_c = self.direction * distance(self.position[1], car.position[1])
					if (dist_to_c > 0 and dist_to_c < 2 * self.size and self.curr_speed > car.curr_speed):
						self.curr_speed = car.curr_speed

		self.position[1] += self.direction * self.curr_speed
		if (self.position[1] > YMAX):
			self.position[1] -= (YMAX-YMIN)
		if (self.position[1] < YMIN):
			self.position[1] += (YMAX-YMIN)
			
		self.distance += self.curr_speed

		if DRAW_FIG:
			rx = XMIN + self.position[0]*(WIDTH/n_lanes) + (WIDTH/n_lanes/4)
			ry = self.position[1] 
			self.rect.set_xy((rx, ry))
			self.annotation.remove()
			self.annotation = ax.text( rx+(WIDTH/n_lanes/6), ry+(WIDTH/n_lanes/4), str(self.car_id), color = 'w')

	# returns booleans for [car crash, pass red, hit pedestrian]
	def check_crash(self):
		global cars
		result = False
		for i in range(1,n_cars):
			if self.collision(cars[i], 1):
				result = True
		return result
		
	# return tuple (g,y,r) for passing lights, e.g. (True, False, False) is returned for passing a green light
	def check_pass_light(self):
		global lights
		result = False
		for light in lights:
			if (light.position < self.position[1] + self.size and
			    light.position > self.position[1] + self.size - self.curr_speed):
				if (light.colorcode == ColorCode.GREEN):
					return (True, False, False)
				elif (light.colorcode == ColorCode.YELLOW):
					return (False, True, False)
				else:
					return (False, False, True)
		return (False, False, False)

	def check_hit_ped(self):
		global peds
		result = False
		for ped in peds:
			if (self.position[0] == np.digitize(ped.position[0], BINS)-1 and
			    ped.position[1] < self.position[1] + self.size and
			    ped.position[1] > self.position[1] + self.size - self.curr_speed):
				return True
		return result

	def collision(self, other, multiplier):
		dist = distance(self.position[1], other.position[1])
		if ( self.position[0] == other.position[0] and 
		     abs(dist) < multiplier * self.size  ):
			return True
		else:
			return False

class Light:
	def __init__(self):
		global lights
		
		while (True):
			self.position = random.uniform(YMIN, YMAX)
			NoCollide = True

			for light in lights:
				if abs(distance(self.position, light.position)) < HEIGHT/n_lights/2:
					NoCollide = False
					break

			if (NoCollide):
				break

		self.colorcode = ColorCode.GREEN
		self.timer = 0

		if DRAW_FIG:
			self.circle = Circle( (XMIN+WIDTH/2,self.position), WIDTH/n_lanes/4, color=Colors[self.colorcode] )
			ax.add_artist(self.circle)

	def __del__(self):
		if DRAW_FIG:
			self.circle.remove()
		
	def update(self):
		if (self.timer > 0):
			self.timer -= 1

		if (self.colorcode == ColorCode.GREEN):
			if (random.random() < 0.002):
				self.colorcode = ColorCode.YELLOW
				self.timer = 30
				if DRAW_FIG:
					self.circle.set_color( Colors[ColorCode.YELLOW])
		elif (self.colorcode == ColorCode.YELLOW):
			if (self.timer == 0):
				self.colorcode = ColorCode.RED
				self.timer = 100
				if DRAW_FIG:
					self.circle.set_color( Colors[ColorCode.RED])
		else:
			if (self.timer == 0):
				self.colorcode = ColorCode.GREEN
				self.timer = 0
				if DRAW_FIG:
					self.circle.set_color( Colors[ColorCode.GREEN])


class Pedestrian:
	def __init__(self, ped_id):
		global ax
		self.ped_id = ped_id
		self.jwalking = False
		if (ped_id % 2 == 0):
			self.position = [XMAX+(WIDTH/n_lanes/2) , random.uniform(YMIN,YMAX) ]
			self.speed = (XMAX-XMIN)/250
			self.direction = +1
		else:
			self.position = [XMIN-(WIDTH/n_lanes/2) , random.uniform(YMIN,YMAX) ]
			self.speed = (XMAX-XMIN)/250
			self.direction = -1

		if DRAW_FIG:
			self.color = Colors[ColorCode.GREEN] 
			self.rect = Rectangle(
				self.position,
				(WIDTH/n_lanes/8),
				(WIDTH/n_lanes/8),
				color=self.color
				)
			ax.add_artist(self.rect)


	def __del__(self):
		if DRAW_FIG:
			self.rect.remove()

	def update(self):
		if (self.jwalking == False):
			self.position[1] += self.direction * self.speed
			if (self.position[1] > YMAX):
				self.position[1] -= (YMAX-YMIN)
			if (self.position[1] < YMIN):
				self.position[1] += (YMAX-YMIN)

			if (random.random() < 0.02):
				self.jwalking = True
		else:
			self.position[0] -= self.direction * self.speed
			if (self.direction == +1 and self.position[0] <= XMIN-(WIDTH/n_lanes/2)):
				self.position[0] = XMIN-(WIDTH/n_lanes/2)
				self.direction = -1
				self.jwalking = False
			if (self.direction == -1 and self.position[0] >= XMAX+(WIDTH/n_lanes/2)):
				self.position[0] = XMAX+(WIDTH/n_lanes/2)
				self.direction = +1
				self.jwalking = False

		if DRAW_FIG:
			self.rect.set_xy( self.position)


######### Methods (API) ############

# update simulation, one step
#returns the most "important" error that happened
#return values :0 - no error
#		1 - Crash
#		2 - Passed Red Light
#		3 - Hit ped
def update(frame_number):
	for car in cars:
		car.update()
	for light in lights:
		light.update()
	for ped in peds:
		ped.update()
	
	if DRAW_FIG:
		plt.draw()
		plt.pause(.00001)

	if cars[0].check_crash() == True:
		#print "Crash!"
		return 1
	if cars[0].check_pass_light() == (False, False, True):
		#print "Passed Red light!"
		return 2
	if cars[0].check_hit_ped() == True:
		#print "Hit Pedestrian!"
		return 3
	return 0

#returns [distance, velocity] in y direction
def get_closest_car_front():
	dist_velocity = [YMAX, -1] 
	for car in lane_cars[cars[0].position[0]]:
		dist = dir_distance(cars[0].position[1], car.position[1])
		if (dist < dist_velocity[0]):
			dist_velocity = [dist, car.speed]
	return dist_velocity

def get_closest_car_back():
	dist_velocity = [YMAX, -1]
        for car in lane_cars[cars[0].position[0]]:
		dist = dir_distance(car.position[1], cars[0].position[1])
		if (dist < dist_velocity[0]):
			dist_velocity = [dist, car.speed]
	return dist_velocity

def get_closest_car_left_front():
	dist_velocity = [YMAX, -1]
	if(cars[0].position[0]!=0):
		for car in lane_cars[cars[0].position[0]-1]:
			dist = dir_distance(cars[0].position[1], car.position[1])
			if (dist < dist_velocity[0]):
				dist_velocity = [dist, car.speed]
	return dist_velocity

def get_closest_car_left_back():
	dist_velocity = [YMAX, -1]
        if(cars[0].position[0]!=0):
                for car in lane_cars[cars[0].position[0]-1]:
			dist = dir_distance(car.position[1], cars[0].position[1])
			if (dist < dist_velocity[0]):
				dist_velocity = [dist, car.speed]
        return dist_velocity

def get_closest_car_right_front():
	dist_velocity = [YMAX, -1]
	if(cars[0].position[0]!=3):
		for car in lane_cars[cars[0].position[0]+1]:
			dist = dir_distance(cars[0].position[1], car.position[1])
			if (dist < dist_velocity[0]):
				dist_velocity = [dist, car.speed]
	return dist_velocity

def get_closest_car_right_back():
	dist_velocity = [YMAX, -1]
        if(cars[0].position[0]!=3):
		for car in lane_cars[cars[0].position[0]+1]:
			dist = dir_distance(car.position[1], cars[0].position[1])
			if (dist < dist_velocity[0]):
				dist_velocity = [dist, car.speed]
	return dist_velocity	
		
		

#returns [ydist, xdist, direction]
#direction of pedestrian's movement: 1 for left, -1 for right
def get_closest_ped():
	dist_lane_dir = [YMAX, -1, -1]
	carx = XMIN + cars[0].position[0]*(WIDTH/n_lanes) + (WIDTH/n_lanes/4)
	for ped in peds:
		if((ped.position[0] > XMIN) and (ped.position[0] < XMAX)):
			if(((ped.direction == -1) and (ped.position[0] < (carx + car_size))) or
			   ((ped.direction == +1) and (ped.position[0] > (carx - car_size)))):
				if (dir_distance(cars[0].position[1], ped.position[1]) < dist_lane_dir[0]):
					dist_lane_dir = [dir_distance(cars[0].position[1], ped.position[1]), 
					                 abs(ped.position[0]-carx), ped.direction]
	return dist_lane_dir
#returns [distance, color 0: G, 1:Y, 2:R]
def get_closest_light():
	dist_color = [YMAX, 0]
	for light in lights:
		if(dir_distance(cars[0].position[1]+cars[0].size, light.position)< dist_color[0]):
			dist_color = [dir_distance(cars[0].position[1]+cars[0].size, light.position), light.colorcode]
	return dist_color

#returns car stats [lane, xpos, ypos, speed]
def get_car_stats():
	return [cars[0].position[0], XMIN + cars[0].position[0]*(WIDTH/n_lanes) + (WIDTH/n_lanes/4),
	        cars[0].position[1], cars[0].speed]

#input: factor to multiply to the speed of the car
def set_car_speed(factor):
	if ((cars[0].speed < (YMAX-YMIN)/180) and (factor > 1)):
		cars[0].speed = (YMAX-YMIN)/180
	else:
		cars[0].speed = cars[0].speed*factor
	if (cars[0].speed > (YMAX-YMIN)/60):
		cars[0].speed = (YMAX-YMIN)/60
	if (cars[0].speed < 0.005):
		cars[0].speed = 0

def reduce_speed(a):
	cars[0].speed -= a
	if cars[0].speed < 0:
		cars[0].speed = 0

def increase_speed(a):
	cars[0].speed += a
	if cars[0].speed > HEIGHT/60:
		cars[0].speed = HEIGHT/60


#input: 1 for left -1 for right
def set_car_lane(lane_dir):
	if ((lane_dir == -1) and (cars[0].position[0] != n_lanes-1)):
		cars[0].position[0] += 1
	elif ((lane_dir == +1) and (cars[0].position[0] != 0)):
		cars[0].position[0] -= 1

# returns total distance traveled by Car 0
def get_total_distance():
	return cars[0].distance

# set_animation should be 0 if the user wants to call update() manually (i.e., during training)
# seed can be passed for reproducable episodes
# draw_fig should be true for visualization
def initialize(draw_fig, set_animation, Seed=None):
	# Create new Figure and an Axes which fills it.
	global fig
	global ax
	global cars
	global lane_cars
	global lights
	global peds
	global animation
	global DRAW_FIG

	if Seed != None:
		random.seed(Seed)
		np.random.seed(Seed)

	DRAW_FIG = draw_fig

	if DRAW_FIG:
		fig = plt.figure(figsize=(7, 7))
		ax = fig.add_axes([0, 0, 1, 1], frameon=False)
		ax.set_xlim(0, 1), ax.set_xticks([])
		ax.set_ylim(0, 1), ax.set_yticks([])

	cars = []
	lights = []
	peds = []
	lane_cars = []

	for i in range(n_lanes):
		lane_cars.append([])
	for i in range(n_cars):
		tmp = Car(i)
		cars.append(tmp)

	for i in range(n_lights):
		tmp = Light()
		lights.append(tmp)

	for i in range(n_peds):
		tmp = Pedestrian(i)
		peds.append(tmp)

	if DRAW_FIG:
		#Drawing the lines on the road, double line for different direction :)
		plt.axvline(x=XMIN           , ymin=YMIN, ymax = YMAX, linewidth=1.5, color='k')
		plt.axvline(x=XMIN+0.25*WIDTH, ymin=YMIN, ymax = YMAX, linewidth=1.5, color='k', linestyle='--')
		plt.axvline(x=XMIN+0.49*WIDTH, ymin=YMIN, ymax = YMAX, linewidth=1.5, color='k')
		plt.axvline(x=XMIN+0.51*WIDTH, ymin=YMIN, ymax = YMAX, linewidth=1.5, color='k')
		plt.axvline(x=XMIN+0.75*WIDTH, ymin=YMIN, ymax = YMAX, linewidth=1.5, color='k', linestyle='--')
		plt.axvline(x=XMAX           , ymin=YMIN, ymax = YMAX, linewidth=1.5, color='k')

		#for light in lights_data:
		for light in lights:
			plt.axhline(xmin=XMIN,xmax=XMAX,y=light.position,linewidth=1.5,color='k')

	if DRAW_FIG:
		# Construct the animation, using the update function as the animation director.
		if set_animation:
			animation = FuncAnimation(fig, update, interval=50)
		else:
			plt.ion() # make the plt.show() function non-blocking

		plt.show()

def stop():
	global animation
	global cars
	global lights
	global peds
	global lane_cars
	cars = []
	lane_cars = []
	lights = []
	peds = []
	if DRAW_FIG:
		#animation.event_source.stop() # BUG: (1) should not be called if set_animation is not set, (2) is this really necessary?
		plt.close()
